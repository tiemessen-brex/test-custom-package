﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(TestingScript))]
public class TestingScriptCustomEditor : Editor
{
    public TestingScript script;		// script reference of the customEditorScript #EDITORSCRIPTNAME#

        // Set the reference
        public void OnEnable()
    {
        script = (TestingScript)target;
    }
	
    // Set custom label and draw default inspector
    public override void OnInspectorGUI() 
    {
        // create label so you know there is a custom editor
        EditorGUILayout.LabelField ("TestingScript have a CustomEditorScript");

        // Show default inspector property editor
        DrawDefaultInspector ();
    }
}
